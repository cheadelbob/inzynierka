-- PriceList
insert into PRICE_LIST(TYPE_OF_SERVICE, MINIMAL_PRICE, MAXIMUM_PRICE) values ('SCALING', 100, 150);
insert into PRICE_LIST(TYPE_OF_SERVICE, MINIMAL_PRICE, MAXIMUM_PRICE) values ('TOOTS_INSPECTION', 0, 50);

-- Address & Patients
insert into ADDRESS(VOIVODESHIP, LOCALITY, ZIP_CODE, STREET, STREET_NUMBER, HOUSE_NUMBER)
values ('ZACHODNIOPOMORSKIE', 'Szczecin', '71-524', 'ul. Wincentego Kadlubka', '32', '2b');
insert into PATIENT(FIRST_NAME, LAST_NAME, PESEL, PHONE, EMAIL, SEX, INFORMATION, ADDRESS_ID)
values ('Jan', 'Kowalski', 93081802434, 512288456, 'jan_kowalski123@wp.pl', 'MALE', 'Pacjent choruje na cukrzyce', 1);
insert into ADDRESS(VOIVODESHIP, LOCALITY, ZIP_CODE, STREET, STREET_NUMBER, HOUSE_NUMBER)
values ('ZACHODNIOPOMORSKIE', 'Wałcz', '78-600', 'ul. Wojska Polskiego', '2D', '12');
insert into PATIENT(FIRST_NAME, LAST_NAME, PESEL, PHONE, EMAIL, SEX, INFORMATION, ADDRESS_ID)
values ('Jakub', 'Malczyński', 93081802435, 505492402, 'j_malczynski@onet.pl', 'MALE', '', 2);

-- Address & Employees
insert into ADDRESS(VOIVODESHIP, LOCALITY, ZIP_CODE, STREET, STREET_NUMBER, HOUSE_NUMBER)
values ('ZACHODNIOPOMORSKIE', 'Szczecin', '71-524', 'ul. Niebuszewo', '10', '15');
insert into EMPLOYEE(FIRST_NAME, LAST_NAME, PWZ, PHONE, PROFFESION, EMAIL, SEX, SALARY, ADDRESS_ID)
values ('Barbara', 'Stankiewicz', 1234567, 400500700, 'DENTIST', 'b_stankiewicz123@gmail.com', 'FEMALE', 7500, 3);
insert into ADDRESS(VOIVODESHIP, LOCALITY, ZIP_CODE, STREET, STREET_NUMBER, HOUSE_NUMBER)
values ('ZACHODNIOPOMORSKIE', 'Szczecin', '71-520', 'ul. Chopina', '55', '708');
insert into EMPLOYEE(FIRST_NAME, LAST_NAME, PWZ, PHONE, PROFFESION, EMAIL, SEX, SALARY, ADDRESS_ID)
values ('Joanna', 'Kolosowska', 5789425, 507897255, 'DENTAL_HYGIENIST', 'joanna85@gmail.com', 'FEMALE', 3700, 4);
insert into ADDRESS(VOIVODESHIP, LOCALITY, ZIP_CODE, STREET, STREET_NUMBER, HOUSE_NUMBER)
values ('ZACHODNIOPOMORSKIE', 'Police', '72-010', 'ul. Energetyków', '4', '7');
insert into EMPLOYEE(FIRST_NAME, LAST_NAME, PWZ, PHONE, PROFFESION, EMAIL, SEX, SALARY, ADDRESS_ID)
values ('Dariusz', 'Zawada', 5789645, 977858656, 'IMPLANTOLOGY', 'darek_zawada@interia.pl', 'MALE', 12000, 5);

-- Meetings
insert into MEETING(DATA, NAME, DURATION, DESCRIPTION)
values ('10.12.2019 19:00:00', 'Zadania na nowy rok', 2,
'Spotkanie dotyczące zadan w przyszym roku. Zostaną omówione poszczególne nowe zadania dla każdego z pracowników.');
insert into MEETING_EMPLOYEE_LIST(MEETING_ID, EMPLOYEE_LIST_ID) values (1, 1);
insert into MEETING_EMPLOYEE_LIST(MEETING_ID, EMPLOYEE_LIST_ID) values (1, 2);

-- Trainings
insert into TRAINING(DATA, NAME, TOWN, DURATION, DESCRIPTION)
values ('11.11.2019 10:00:00', 'Asystowanie w implantologii', 'Warszawa', 12,
'Profesjonalna asysta przy zabiegach implantologicznych.');
insert into TRAINING_EMPLOYEE_LIST(TRAINING_ID, EMPLOYEE_LIST_ID) values (1, 1);
insert into TRAINING_EMPLOYEE_LIST(TRAINING_ID, EMPLOYEE_LIST_ID) values (1, 2);
insert into TRAINING_EMPLOYEE_LIST(TRAINING_ID, EMPLOYEE_LIST_ID) values (1, 3);
insert into TRAINING(DATA, NAME, TOWN, DURATION, DESCRIPTION)
values ('17.11.2019 08:00:00', 'Kurs ortodontyczny II stopnia', 'Łódź', 12,
'Kurs obejmuje teoretyczne podstawy biomechaniki i ich aplikację w praktyce: ' ||
'wektory sił, momenty sił, ich wpływ na zachowanie przemieszczanych zębów.');
insert into TRAINING_EMPLOYEE_LIST(TRAINING_ID, EMPLOYEE_LIST_ID) values (1, 1);

-- Visits
insert into VISIT(DATA, DESCRIPTION, EMPLOYEE_ID, CUSTOMER_ID)
values ('04.08.2019 20:10:00', 'Ekstrakcja zęba', 1, 1);
insert into VISIT(DATA, DESCRIPTION, EMPLOYEE_ID, CUSTOMER_ID)
values ('12.12.2019 09:30:00', 'Piaskowanie', 3, 2);



