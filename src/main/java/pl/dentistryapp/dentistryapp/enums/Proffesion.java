package pl.dentistryapp.dentistryapp.enums;

public enum Proffesion {
    DENTAL_ASSISTANT, DENTAL_HYGIENIST,
    SURGEON, IMPLANTOLOGY, ENDODONTIST, DENTIST
}
