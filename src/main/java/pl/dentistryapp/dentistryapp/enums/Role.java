package pl.dentistryapp.dentistryapp.enums;

public enum Role {
    USER, ADMIN, SUPERADMIN
}
