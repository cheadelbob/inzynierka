package pl.dentistryapp.dentistryapp.enums;

public enum Sex {
    MALE, FEMALE
}
