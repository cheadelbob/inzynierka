package pl.dentistryapp.dentistryapp.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dentistryapp.dentistryapp.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    List<Employee> findAllByFirstName(String firstName);

    List<Employee> findAllByLastName(String lastName);

    Optional<Employee> findByPwz(Integer pwz);
}