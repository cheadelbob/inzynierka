package pl.dentistryapp.dentistryapp.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dentistryapp.dentistryapp.model.Training;

public interface TrainingRepository extends CrudRepository<Training, Long> {

}