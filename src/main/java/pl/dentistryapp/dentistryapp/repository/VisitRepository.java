package pl.dentistryapp.dentistryapp.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dentistryapp.dentistryapp.model.Visit;

public interface VisitRepository extends CrudRepository<Visit, Long> {

}