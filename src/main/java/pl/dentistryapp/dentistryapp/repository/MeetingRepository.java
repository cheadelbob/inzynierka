package pl.dentistryapp.dentistryapp.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dentistryapp.dentistryapp.model.Meeting;

public interface MeetingRepository extends CrudRepository<Meeting, Long> {

}