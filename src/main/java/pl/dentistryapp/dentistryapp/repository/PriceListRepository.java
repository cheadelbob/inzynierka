package pl.dentistryapp.dentistryapp.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dentistryapp.dentistryapp.model.PriceList;

public interface PriceListRepository extends CrudRepository<PriceList, Long> {

}