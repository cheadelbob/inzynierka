package pl.dentistryapp.dentistryapp.repository;

import org.springframework.data.repository.CrudRepository;
import pl.dentistryapp.dentistryapp.model.Patient;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends CrudRepository<Patient, Long> {

    List<Patient> findAllByFirstName(String firstName);

    List<Patient> findAllByLastName(String lastName);

    Optional<Patient> findByPesel(String pesel);

}