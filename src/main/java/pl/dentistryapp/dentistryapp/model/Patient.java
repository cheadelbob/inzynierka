package pl.dentistryapp.dentistryapp.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.pl.PESEL;
import pl.dentistryapp.dentistryapp.enums.Sex;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@NoArgsConstructor
@Data
public class Patient {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @NotNull
    @Size(min = 2, max = 20)
    @Column
    private String firstName;

    @NotNull
    @Size(min = 2, max = 20)
    @Column
    private String lastName;

    @PESEL
    @Column
    @NotNull
    private String pesel;

    @Column
    private Integer phone;

    @Email
    @Column
    private String email;

    @Column
    @Lob
    private String information;

    @Column
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;
}
