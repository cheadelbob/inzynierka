package pl.dentistryapp.dentistryapp.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


@Entity
@Data
public class Meeting {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column
    @NotNull
    private String data;

    @Column
    private String name;

    @Column
    private Integer duration;

    @Column
    @Lob
    private String description;

    @ManyToMany(cascade = CascadeType.DETACH)
    private List<Employee> employeeList;
}
