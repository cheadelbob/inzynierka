package pl.dentistryapp.dentistryapp.model;

import lombok.Data;
import pl.dentistryapp.dentistryapp.enums.TypeOfService;

import javax.persistence.*;

@Data
@Entity
public class PriceList {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column
    private TypeOfService typeOfService;

    @Column
    private Integer minimalPrice;

    @Column
    private Integer maximumPrice;
}
