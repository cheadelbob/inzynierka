package pl.dentistryapp.dentistryapp.model;

import lombok.Data;
import pl.dentistryapp.dentistryapp.enums.Voivodeship;

import javax.persistence.*;

@Entity
@Data
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column
    private Voivodeship voivodeship;

    @Column
    private String locality;

    @Column
    private String zipCode;

    @Column
    private String street;

    @Column
    private String streetNumber;

    @Column
    private String houseNumber;
}
