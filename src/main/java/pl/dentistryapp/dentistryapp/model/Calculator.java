package pl.dentistryapp.dentistryapp.model;

import lombok.Data;

@Data
public class Calculator {

    private Integer bodyWeight;
    private Integer dose;
}
