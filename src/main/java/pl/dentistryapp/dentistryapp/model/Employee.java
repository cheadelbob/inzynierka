package pl.dentistryapp.dentistryapp.model;

import lombok.Data;
import pl.dentistryapp.dentistryapp.enums.Proffesion;
import pl.dentistryapp.dentistryapp.enums.Sex;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
public class Employee {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @NotNull
    @Size(min = 2, max = 20)
    @Column
    private String firstName;

    @NotNull
    @Size(min = 2, max = 20)
    @Column
    private String lastName;

    @Enumerated(EnumType.STRING)
    @Column
    private Sex sex;

    @Column
    @NotNull
    private Integer phone;

    @NotNull
    @Column(unique = true)
    private Integer pwz;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column
    private Proffesion proffesion;

    @Email
    @Column
    private String email;

    @Column
    private double salary;

    @OneToOne(cascade = {CascadeType.ALL})
    private Address address;
}
