package pl.dentistryapp.dentistryapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dentistryapp.dentistryapp.model.Training;
import pl.dentistryapp.dentistryapp.repository.TrainingRepository;

import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin
@RestController
public class TrainingController {

    @Autowired
    public TrainingRepository trainingRepository;

    @GetMapping(value = "trainings", produces = APPLICATION_JSON_VALUE)
    public @Valid Iterable<Training> getTraining() {
        return trainingRepository.findAll();
    }

    @GetMapping(value = "trainings/{id}", produces = APPLICATION_JSON_VALUE)
    public Optional<Training> getTraining(@PathVariable Long id) {
        return trainingRepository.findById(id);
    }

    @PostMapping(value = "trainings", produces = APPLICATION_JSON_VALUE)
    public @Valid Training addTraining(@Valid @RequestBody Training training) {
        return trainingRepository.save(training);
    }

    @DeleteMapping(value = "trainings/{id}", produces = APPLICATION_JSON_VALUE)
    public String deleteTraining(@PathVariable Long id) {
        trainingRepository.deleteById(id);
        return "Training with id(" + id + ") was deleted";
    }

    @PutMapping(value = "trainings", produces = APPLICATION_JSON_VALUE)
    public @Valid Training updateTraining(@Valid @RequestBody Training training) {
        return trainingRepository.save(training);
    }
}
