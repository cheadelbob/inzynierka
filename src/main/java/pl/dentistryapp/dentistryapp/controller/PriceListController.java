package pl.dentistryapp.dentistryapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dentistryapp.dentistryapp.model.PriceList;
import pl.dentistryapp.dentistryapp.repository.PriceListRepository;

import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin
@RestController
public class PriceListController {

    @Autowired
    public PriceListRepository priceListRepository;

    @GetMapping(value = "priceLists", produces = APPLICATION_JSON_VALUE)
    public @Valid Iterable<PriceList> getPriceLists() {
        return priceListRepository.findAll();
    }

    @GetMapping(value = "priceLists/{id}", produces = APPLICATION_JSON_VALUE)
    public Optional<PriceList> getPriceLists(@PathVariable Long id) {
        return priceListRepository.findById(id);
    }

    @PostMapping(value = "priceLists", produces = APPLICATION_JSON_VALUE)
    public @Valid PriceList addPriceLists(@Valid @RequestBody PriceList PriceList) {
        return priceListRepository.save(PriceList);
    }

    @DeleteMapping(value = "priceLists/{id}", produces = APPLICATION_JSON_VALUE)
    public String deletePriceLists(@PathVariable Long id) {
        priceListRepository.deleteById(id);
        return "Position with id(" + id + ") was deleted from PriceList";
    }

    @PutMapping(value = "priceLists", produces = APPLICATION_JSON_VALUE)
    public @Valid PriceList updatePriceLists(@Valid @RequestBody PriceList PriceList) {
        return priceListRepository.save(PriceList);
    }
}
