package pl.dentistryapp.dentistryapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dentistryapp.dentistryapp.model.Patient;
import pl.dentistryapp.dentistryapp.repository.CustomerRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin
@RestController
public class CustomerController {

    @Autowired
    public CustomerRepository customerRepository;

    @GetMapping(value = "customers", produces = APPLICATION_JSON_VALUE)
    public @Valid Iterable<Patient> getCustomers() {
        return customerRepository.findAll();
    }

    @GetMapping(value = "customers/{id}", produces = APPLICATION_JSON_VALUE)
    public Optional<Patient> getCustomers(@PathVariable Long id) {
        return customerRepository.findById(id);
    }

    @PostMapping(value = "customers", produces = APPLICATION_JSON_VALUE)
    public @Valid Patient addCustomers(@Valid @RequestBody Patient patient) {
        return customerRepository.save(patient);
    }

    @DeleteMapping(value = "customers/{id}", produces = APPLICATION_JSON_VALUE)
    public String deleteCustomers(@PathVariable Long id) {
        customerRepository.deleteById(id);
        return "Visit with id(" + id + ") was deleted";
    }

    @PutMapping(value = "customers", produces = APPLICATION_JSON_VALUE)
    public @Valid Patient updateCustomers(@Valid @RequestBody Patient patient) {
        return customerRepository.save(patient);
    }

    @GetMapping(value = "customers/findByFirstName", produces = APPLICATION_JSON_VALUE)
    public List<Patient> getEmployeeListByFirstName(@RequestParam String firstName) {
        return customerRepository.findAllByFirstName(firstName);
    }

    @GetMapping(value = "customers/findByLastName", produces = APPLICATION_JSON_VALUE)
    public List<Patient> getEmployeeListByLastName(@RequestParam String lastName) {
        return customerRepository.findAllByLastName(lastName);
    }

    @GetMapping(value = "customers/findByPesel", produces = APPLICATION_JSON_VALUE)
    public Optional<Patient> getEmployeeByPesel(@RequestParam String pesel) {
        return customerRepository.findByPesel(pesel);
    }
}
