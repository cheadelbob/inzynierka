package pl.dentistryapp.dentistryapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dentistryapp.dentistryapp.model.Employee;
import pl.dentistryapp.dentistryapp.model.Patient;
import pl.dentistryapp.dentistryapp.model.Visit;
import pl.dentistryapp.dentistryapp.repository.CustomerRepository;
import pl.dentistryapp.dentistryapp.repository.EmployeeRepository;
import pl.dentistryapp.dentistryapp.repository.VisitRepository;

import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin
@RestController
public class VisitController {

    @Autowired
    public VisitRepository visitRepository;

    @Autowired
    public CustomerRepository customerRepository;

    @Autowired
    public EmployeeRepository employeeRepository;

    @GetMapping(value = "visits", produces = APPLICATION_JSON_VALUE)
    public @Valid Iterable<Visit> getVisit() {
        return visitRepository.findAll();
    }

    @GetMapping(value = "visits/{id}", produces = APPLICATION_JSON_VALUE)
    public Optional<Visit> getVisit(@PathVariable Long id) {
        return visitRepository.findById(id);
    }

    @PostMapping(value = "visits", produces = APPLICATION_JSON_VALUE)
    public @Valid Visit addVisit(@Valid @RequestBody Visit visit) {
        Optional<Patient> customerRepositoryById = customerRepository.findById(visit.getPatient().getId());
        visit.setPatient(customerRepositoryById.get());

        Optional<Employee> employeeRepositoryById = employeeRepository.findById(visit.getEmployee().getId());
        visit.setEmployee(employeeRepositoryById.get());

        return visitRepository.save(visit);
    }

    @DeleteMapping(value = "visits/{id}", produces = APPLICATION_JSON_VALUE)
    public String deleteVisit(@PathVariable Long id) {
        visitRepository.deleteById(id);
        return "Visit with id(" + id + ") was deleted";
    }

    @PutMapping(value = "visits", produces = APPLICATION_JSON_VALUE)
    public @Valid Visit updateVisit(@Valid @RequestBody Visit visit) {
        Optional<Patient> customerRepositoryById = customerRepository.findById(visit.getPatient().getId());
        visit.setPatient(customerRepositoryById.get());

        Optional<Employee> employeeRepositoryById = employeeRepository.findById(visit.getEmployee().getId());
        visit.setEmployee(employeeRepositoryById.get());
        return visitRepository.save(visit);
    }
}
