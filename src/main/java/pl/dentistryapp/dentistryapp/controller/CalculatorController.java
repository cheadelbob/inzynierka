package pl.dentistryapp.dentistryapp.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.dentistryapp.dentistryapp.model.Calculator;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@CrossOrigin
@RestController
public class CalculatorController {

    @PostMapping(value = "calculator", produces = APPLICATION_JSON_VALUE)
    public Integer doCalculate(@Valid @RequestBody Calculator calculator) {
        Integer bodyWeight = calculator.getBodyWeight();
        Integer dose = calculator.getDose();
        return (bodyWeight / dose);
    }
}
