package pl.dentistryapp.dentistryapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dentistryapp.dentistryapp.model.Employee;
import pl.dentistryapp.dentistryapp.model.Meeting;
import pl.dentistryapp.dentistryapp.repository.EmployeeRepository;
import pl.dentistryapp.dentistryapp.repository.MeetingRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin
@RestController
public class MeetingController {

    @Autowired
    public MeetingRepository meetingRepository;

    @Autowired
    public EmployeeRepository employeeRepository;

    @GetMapping(value = "meetings", produces = APPLICATION_JSON_VALUE)
    public @Valid Iterable<Meeting> getMeeting() {
        return meetingRepository.findAll();
    }

    @GetMapping(value = "meetings/{id}", produces = APPLICATION_JSON_VALUE)
    public Optional<Meeting> getMeeting(@PathVariable Long id) {
        return meetingRepository.findById(id);
    }

    @PostMapping(value = "meetings", produces = APPLICATION_JSON_VALUE)
    public @Valid Meeting addMeeting(@Valid @RequestBody Meeting meeting) {
        List<Employee> employeeList = meeting.getEmployeeList();
        List<Employee> newEmployeeList = new ArrayList<>();
        for (Employee employee : employeeList) {
            Optional<Employee> byId = employeeRepository.findById(employee.getId());
            newEmployeeList.add(byId.get());
        }
        meeting.setEmployeeList(newEmployeeList);
        return meetingRepository.save(meeting);
    }

    @DeleteMapping(value = "meetings/{id}", produces = APPLICATION_JSON_VALUE)
    public String deleteMeeting(@PathVariable Long id) {
        meetingRepository.deleteById(id);
        return "Meeting with id(" + id + ") was deleted";
    }

    @PutMapping(value = "meetings", produces = APPLICATION_JSON_VALUE)
    public @Valid Meeting updateMeeting(@Valid @RequestBody Meeting meeting) {
        return meetingRepository.save(meeting);
    }
}
