package pl.dentistryapp.dentistryapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dentistryapp.dentistryapp.model.Employee;
import pl.dentistryapp.dentistryapp.repository.EmployeeRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping(value = "employees", produces = APPLICATION_JSON_VALUE)
    public Iterable<Employee> getEmployee() {
        return employeeRepository.findAll();
    }

    @GetMapping(value = "employees/{id}", produces = APPLICATION_JSON_VALUE)
    public Optional<Employee> getEmployee(@PathVariable Long id) {
        return employeeRepository.findById(id);
    }

    @PostMapping(value = "employees", produces = APPLICATION_JSON_VALUE)
    public @Valid Employee addEmployee(@Valid @RequestBody Employee employee) throws Exception {
        int length = String.valueOf(employee.getPwz()).length();
        if (length != 7) {
            throw new Exception("The PWZ number must have 7 digits");
        }
        return employeeRepository.save(employee);
    }

    @DeleteMapping(value = "employees/{id}", produces = APPLICATION_JSON_VALUE)
    public String deleteEmployee(@PathVariable Long id) {
        employeeRepository.deleteById(id);
        return "employee with id(" + id + ") was deleted";
    }

    @PutMapping(value = "employees", produces = APPLICATION_JSON_VALUE)
    public @Valid Employee updateEmployee(@Valid @RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @GetMapping(value = "employees/findByFirstName", produces = APPLICATION_JSON_VALUE)
    public List<Employee> getEmployeeListByFirstName(@RequestParam String firstName) {
        return employeeRepository.findAllByFirstName(firstName);
    }

    @GetMapping(value = "employees/findByLastName", produces = APPLICATION_JSON_VALUE)
    public List<Employee> getEmployeeListByLastName(@RequestParam String lastName) {
        return employeeRepository.findAllByLastName(lastName);
    }

    @GetMapping(value = "employees/findByPwz", produces = APPLICATION_JSON_VALUE)
    public Optional<Employee> getEmployeeByPwz(@RequestParam Integer pwz) {
        return employeeRepository.findByPwz(pwz);
    }
}
