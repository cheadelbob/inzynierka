package pl.dentistryapp.dentistryapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "pl.dentistryapp.dentistryapp.repository")
public class DentistryappApplication {

    public static void main(String[] args) {

        SpringApplication.run(DentistryappApplication.class, args);
    }
}